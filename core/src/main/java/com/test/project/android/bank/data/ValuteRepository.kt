package com.test.project.android.bank.data

import com.test.project.android.bank.domain.Valute

class ValuteRepository(private val dataSource: ValuteDataSource) {
    suspend fun addValute(valute: Valute) =
            dataSource.add(valute)

    suspend fun getValutes() = dataSource.get()
    suspend fun getValute(code: String) = dataSource.getByCode(code)
}