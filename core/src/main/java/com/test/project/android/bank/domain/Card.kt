package com.test.project.android.bank.domain

data class Card(
        val cardNumber: String,
        val type: String,
        val cardholderName: String,
        val valid: String,
        val balance: Double,
        var transactionHistory: List<Transaction>
) {

    companion object {
        val EMPTY = Card("0000 0000 0000 0000", "-", "----- -----", "--/--", 0.0, mutableListOf())
    }
}