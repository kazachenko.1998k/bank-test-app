package com.test.project.android.bank.interactors

import com.test.project.android.bank.data.ValuteRepository

class GetValute(private val valuteRepository: ValuteRepository) {
    suspend operator fun invoke(code: String) =
            valuteRepository.getValute(code)
}