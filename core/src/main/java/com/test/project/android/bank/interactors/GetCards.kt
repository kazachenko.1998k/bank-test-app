package com.test.project.android.bank.interactors

import com.test.project.android.bank.data.CardRepository

class GetCards (private val cardRepository: CardRepository) {
    suspend operator fun invoke() =
            cardRepository.getCards()
}