package com.test.project.android.bank.interactors

import com.test.project.android.bank.data.CardRepository
import com.test.project.android.bank.domain.Card

class AddCard (private val cardRepository: CardRepository) {
    suspend operator fun invoke(card: Card) =
            cardRepository.addCard(card)
}