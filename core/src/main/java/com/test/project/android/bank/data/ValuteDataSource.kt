package com.test.project.android.bank.data

import com.test.project.android.bank.domain.Valute

interface ValuteDataSource {
    suspend fun add(valute: Valute)

    suspend fun get(): List<Valute>?

    suspend fun getByCode(code:String): Valute?

}