package com.test.project.android.bank.interactors

import com.test.project.android.bank.data.ValuteRepository
import com.test.project.android.bank.domain.Valute

class AddValute (private val valuteRepository: ValuteRepository) {
    suspend operator fun invoke(valute: Valute) =
            valuteRepository.addValute(valute)
}