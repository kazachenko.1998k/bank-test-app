package com.test.project.android.bank.data

import com.test.project.android.bank.domain.Card
import com.test.project.android.bank.domain.Transaction

interface TransactionDataSource {
    suspend fun add(transaction: Transaction, card: Card)
    suspend fun get(card: Card): List<Transaction>
    suspend fun clear(card: Card)
}