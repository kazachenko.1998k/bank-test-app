package com.test.project.android.bank.interactors

import com.test.project.android.bank.data.ValuteRepository

class GetValutes (private val valuteRepository: ValuteRepository) {
    suspend operator fun invoke() =
            valuteRepository.getValutes()
}