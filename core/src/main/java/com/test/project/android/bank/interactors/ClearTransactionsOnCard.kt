package com.test.project.android.bank.interactors

import com.test.project.android.bank.data.TransactionRepository
import com.test.project.android.bank.domain.Card

class ClearTransactionsOnCard (private val transactionRepository: TransactionRepository) {
    suspend operator fun invoke(card: Card) =
            transactionRepository.clearTransactionsOnCard(card)
}