package com.test.project.android.bank.data

import com.test.project.android.bank.domain.Card
import com.test.project.android.bank.domain.Transaction

class TransactionRepository (private val dataSource: TransactionDataSource) {
    suspend fun addTransaction(document: Transaction, card: Card) =
            dataSource.add(document, card)

    suspend fun getTransactions(card: Card) = dataSource.get(card)

    suspend fun clearTransactionsOnCard(card: Card) = dataSource.clear(card)

}