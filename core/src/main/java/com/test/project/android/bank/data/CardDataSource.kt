package com.test.project.android.bank.data

import com.test.project.android.bank.domain.Card

interface CardDataSource {
    suspend fun add(card: Card)
    suspend fun get(): List<Card>?
    suspend fun getByNumber(number: String): Card?
}