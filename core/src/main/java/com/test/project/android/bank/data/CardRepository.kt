package com.test.project.android.bank.data

import com.test.project.android.bank.domain.Card

class CardRepository (private val dataSource: CardDataSource) {
    suspend fun addCard(card: Card) =
            dataSource.add(card)

    suspend fun getCards() = dataSource.get()

}