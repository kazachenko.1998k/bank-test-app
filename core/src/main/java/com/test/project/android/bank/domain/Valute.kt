package com.test.project.android.bank.domain

data class Valute(
        val id: String,
        val charCode: String,
        val numCode: String,
        val nominal: Int,
        val name: String,
        val value: Double?,
        val previous: Double?
) {

    fun symbol(): String {
        return when (charCode) {
            GBP.second -> GBP.first
            EUR.second -> EUR.first
            RUB.second -> RUB.first
            USD.second -> USD.first
            else -> "$charCode "
        }
    }

    fun default(): String {
        return USD.first
    }

    companion object {
        val EMPTY = Valute("", "", "", 0, "", 0.0, 0.0)
        val GBP = Pair("£", "GBP")
        val EUR = Pair("€", "EUR")
        val RUB = Pair("₽", "RUB")
        val USD = Pair("$", "USD")
    }
}