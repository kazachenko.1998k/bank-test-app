package com.test.project.android.bank.interactors

import com.test.project.android.bank.data.TransactionRepository
import com.test.project.android.bank.domain.Card
import com.test.project.android.bank.domain.Transaction

class AddTransaction (private val transactionRepository: TransactionRepository) {
    suspend operator fun invoke(transaction: Transaction, card: Card) =
            transactionRepository.addTransaction(transaction, card)
}