package com.test.project.android.bank.domain

data class Transaction(
        val title: String,
        val iconUrl: String?,
        val date: String,
        val amount: String
)
