package com.test.project.android.bank.framework.db

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey

@Entity(tableName = "transaction", foreignKeys = [ForeignKey(entity = CardEntity::class,
        parentColumns = arrayOf("card_number"),
        childColumns = arrayOf("card_id"),
        onDelete = ForeignKey.CASCADE)])
data class TransactionEntity(
        @PrimaryKey(autoGenerate = true)
        val id: Int = 0,

        @ColumnInfo(name = "card_id")
        val cardId: String,

        @ColumnInfo(name = "title")
        val title: String,

        @ColumnInfo(name = "icon_url")
        val iconUrl: String?,

        @ColumnInfo(name = "date")
        val date: String,

        @ColumnInfo(name = "amount")
        val amount: String
)
