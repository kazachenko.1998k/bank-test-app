package com.test.project.android.bank.framework.db.ds

import android.content.Context
import com.test.project.android.bank.data.ValuteDataSource
import com.test.project.android.bank.domain.Valute
import com.test.project.android.bank.framework.db.BankDatabase
import com.test.project.android.bank.framework.db.ValuteEntity

class RoomValuteDataSource(context: Context) : ValuteDataSource {

    private val valuteDao = BankDatabase.getInstance(context).valuteDao()


    override suspend fun add(valute: Valute) = valuteDao.addValute(ValuteEntity(
            value = valute.value ?: -1.0,
            charCode = valute.charCode,
            id = valute.id,
            name = valute.name,
            nominal = valute.nominal,
            numCode = valute.numCode,
            previous = valute.previous ?: -1.0
    ))

    override suspend fun get(): List<Valute>? {
        val usd = valuteDao.getValute(Valute.USD.second)
        return valuteDao.getValutes().map { valute ->
            Valute(
                    value = usd!!.value!!/valute.value!!,
                    charCode = valute.charCode,
                    id = valute.id,
                    name = valute.name,
                    nominal = valute.nominal,
                    numCode = valute.numCode,
                    previous = valute.previous
            )
        }

    }

    override suspend fun getByCode(code: String): Valute? {
        val valute = valuteDao.getValute(code) ?: return null
        val usd = valuteDao.getValute(Valute.USD.second)
        return Valute(
                value = usd!!.value!!/valute.value!!,
                charCode = valute.charCode,
                id = valute.id,
                name = valute.name,
                nominal = valute.nominal,
                numCode = valute.numCode,
                previous = valute.previous
        )
    }

}