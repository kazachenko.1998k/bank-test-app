package com.test.project.android.bank.framework.network.models

import com.squareup.moshi.Json

data class ValuteNet(
        @Json(name = "CharCode")
        val CharCode: String = "",

        @Json(name = "ID")
        val ID: String = "",

        @Json(name = "NumCode")
        val NumCode: String = "",

        @Json(name = "Nominal")
        val Nominal: Int = 0,

        @Json(name = "Name")
        val Name: String = "",

        @Json(name = "Value")
        val Value: Double = 0.0,

        @Json(name = "Previous")
        val Previous: Double = 0.0
)