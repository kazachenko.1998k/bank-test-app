package com.test.project.android.bank.framework

import android.app.Application
import com.test.project.android.bank.data.*
import com.test.project.android.bank.framework.db.ds.RoomCardDataSource
import com.test.project.android.bank.framework.db.ds.RoomTransactionDataSource
import com.test.project.android.bank.framework.db.ds.RoomValuteDataSource
import com.test.project.android.bank.framework.network.ds.NetCardDataSource
import com.test.project.android.bank.framework.network.ds.NetValuteDataSource
import com.test.project.android.bank.interactors.*

class BankApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        val roomTransactionDataSource = RoomTransactionDataSource(applicationContext)
        val cardRepository = CardRepository(CardDataSourceImpl(NetCardDataSource(), RoomCardDataSource(applicationContext), roomTransactionDataSource))
        val transactionRepository = TransactionRepository(roomTransactionDataSource)
        val valuteRepository = ValuteRepository(ValuteDataSourceImpl(NetValuteDataSource(), RoomValuteDataSource(applicationContext)))

        BankViewModelFactory.inject(
                this,
                Interactors(
                        AddCard(cardRepository),
                        AddTransaction(transactionRepository),
                        AddValute(valuteRepository),
                        ClearTransactionsOnCard(transactionRepository),
                        GetCards(cardRepository),
                        GetTransactions(transactionRepository),
                        GetValutes(valuteRepository),
                        GetValute(valuteRepository)
                )
        )
    }

}