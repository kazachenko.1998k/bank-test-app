package com.test.project.android.bank.framework.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

@Database(
        entities = [TransactionEntity::class, CardEntity::class, ValuteEntity::class],
        version = 1,
        exportSchema = false
)
abstract class BankDatabase : RoomDatabase() {

    companion object {

        private const val DATABASE_NAME = "bank.db"

        private var instance: BankDatabase? = null

        private fun create(context: Context): BankDatabase =
                Room.databaseBuilder(context, BankDatabase::class.java, DATABASE_NAME)
                        .fallbackToDestructiveMigration()
                        .build()


        fun getInstance(context: Context): BankDatabase =
                (instance ?: create(context)).also { instance = it }
    }

    abstract fun transactionDao(): TransactionDao

    abstract fun cardDao(): CardDao

    abstract fun valuteDao(): ValuteDao

}