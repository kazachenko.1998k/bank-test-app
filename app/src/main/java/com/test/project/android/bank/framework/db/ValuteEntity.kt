package com.test.project.android.bank.framework.db

import androidx.room.*

@Entity(tableName = "valute", indices = [Index(value = ["char_code"], unique = true)])
data class ValuteEntity(
        @PrimaryKey
        @ColumnInfo(name = "char_code")
        val charCode: String,

        @ColumnInfo(name = "id")
        val id: String,

        @ColumnInfo(name = "num_code")
        val numCode: String,

        @ColumnInfo(name = "nominal")
        val nominal: Int,

        @ColumnInfo(name = "name")
        val name: String,

        @ColumnInfo(name = "value")
        val value: Double?,

        @ColumnInfo(name = "previous")
        val previous: Double?

)