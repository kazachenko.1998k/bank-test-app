package com.test.project.android.bank.framework.db.ds

import android.content.Context
import com.test.project.android.bank.data.CardDataSource
import com.test.project.android.bank.domain.Card
import com.test.project.android.bank.domain.Transaction
import com.test.project.android.bank.framework.db.BankDatabase
import com.test.project.android.bank.framework.db.CardEntity

class RoomCardDataSource(context: Context) : CardDataSource {

    private val cardDao = BankDatabase.getInstance(context).cardDao()

    override suspend fun add(card: Card) =
            cardDao.addCard(CardEntity(
                    cardNumber = card.cardNumber,
                    balance = card.balance,
                    cardholderName = card.cardholderName,
                    type = card.type,
                    valid = card.valid
            ))

    override suspend fun get(): List<Card> {
        val result = cardDao.getCards().map { card ->
            Card(cardNumber = card.cardNumber,
                    balance = card.balance,
                    cardholderName = card.cardholderName,
                    type = card.type,
                    valid = card.valid, transactionHistory = mutableListOf())
        }
        result.forEach {
            it.transactionHistory = cardDao.getTransactions(it.cardNumber).map { transaction ->
                Transaction(
                        amount = transaction.amount,
                        date = transaction.date,
                        iconUrl = transaction.iconUrl,
                        title = transaction.title
                )
            }
        }
        return result
    }

    override suspend fun getByNumber(number: String): Card {
        val card = cardDao.getCard(number)
        card?: return Card.EMPTY
        val result = Card(cardNumber = card.cardNumber,
                balance = card.balance,
                cardholderName = card.cardholderName,
                type = card.type,
                valid = card.valid, transactionHistory = mutableListOf())
        result.transactionHistory = cardDao.getTransactions(result.cardNumber).map { transaction ->
            Transaction(
                    amount = transaction.amount,
                    date = transaction.date,
                    iconUrl = transaction.iconUrl,
                    title = transaction.title
            )
        }
        return result
    }
}