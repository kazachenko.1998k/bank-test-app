package com.test.project.android.bank.framework.db

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "cards")
data class CardEntity(
        @PrimaryKey
        @ColumnInfo(name = "card_number")
        val cardNumber: String,

        @ColumnInfo(name = "type")
        val type: String,

        @ColumnInfo(name = "cardholder_name")
        val cardholderName: String,

        @ColumnInfo(name = "valid")
        val valid: String,

        @ColumnInfo(name = "balance")
        val balance: Double
)