package com.test.project.android.bank.framework.db

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy.REPLACE
import androidx.room.Query

@Dao
interface TransactionDao {

    @Insert(onConflict = REPLACE)
    suspend fun addTransaction(transactionEntity: TransactionEntity)

    @Query("SELECT * FROM `transaction` WHERE card_id = :cardId")
    suspend fun getTransactions(cardId: String): List<TransactionEntity>

    @Query("DELETE FROM `transaction` WHERE card_id = :cardId")
    suspend fun clearTransactionsOnCard(cardId: String)
}