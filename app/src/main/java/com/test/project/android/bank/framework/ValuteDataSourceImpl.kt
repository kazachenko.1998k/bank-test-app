package com.test.project.android.bank.framework

import com.test.project.android.bank.data.ValuteDataSource
import com.test.project.android.bank.domain.Valute
import com.test.project.android.bank.framework.db.ds.RoomValuteDataSource
import com.test.project.android.bank.framework.network.ds.NetValuteDataSource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class ValuteDataSourceImpl(private val netValuteDataSource: NetValuteDataSource, private val roomValuteDataSource: RoomValuteDataSource) : ValuteDataSource {

    override suspend fun add(valute: Valute) {
        roomValuteDataSource.add(valute)
    }

    override suspend fun get(): List<Valute>? {
        return withContext(Dispatchers.IO) {
            getNet()
            roomValuteDataSource.get()
        }
    }

    override suspend fun getByCode(code: String): Valute? {
        return withContext(Dispatchers.IO) {
            getNet()
            roomValuteDataSource.getByCode(code)
        }
    }

    suspend fun getNet() {
        val net = netValuteDataSource.get()
        if (!net.isNullOrEmpty()) {
            net.forEach { valute ->
                add(valute)
            }
            add(Valute(
                    "R00000",
                    "RUB",
                    "001",
                    1,
                    "Российский рубль",
                    1.0,
                    1.0
            ))
        }
    }

}