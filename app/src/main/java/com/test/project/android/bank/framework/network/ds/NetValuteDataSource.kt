package com.test.project.android.bank.framework.network.ds

import com.test.project.android.bank.data.ValuteDataSource
import com.test.project.android.bank.domain.Valute
import com.test.project.android.bank.framework.network.NetworkModule
import com.test.project.android.bank.framework.network.api.ValuteApi

class NetValuteDataSource : ValuteDataSource {

    private val valuteApi by lazy { NetworkModule.retrofit(NetworkModule.VALUTE_URL).create(ValuteApi::class.java) }

    override suspend fun add(valute: Valute) {
        TODO("Not yet implemented")
    }

    override suspend fun get(): List<Valute>? = valuteApi.getAllValutes()?.await()?.Valute?.values?.map { valute ->
        Valute(
                value = valute.Value,
                charCode = valute.CharCode,
                id = valute.ID,
                name = valute.Name,
                nominal = valute.Nominal,
                numCode = valute.NumCode,
                previous = valute.Previous
        )
    }

    override suspend fun getByCode(code: String): Valute? = get()?.find { it.charCode == code }
            ?: Valute.EMPTY

}