package com.test.project.android.bank.framework.network

import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import java.security.cert.CertificateException
import javax.net.ssl.HostnameVerifier
import javax.net.ssl.SSLContext
import javax.net.ssl.TrustManager
import javax.net.ssl.X509TrustManager

object NetworkModule {
    const val VALUTE_URL = "https://www.cbr-xml-daily.ru/"
    const val CARDS_URL = "https://hr.peterpartner.net/test/android/v1/"

    private fun okHttpSsl(interceptor: Interceptor?): OkHttpClient {
        try {
            val trustAllCerts = arrayOf<TrustManager>(object : X509TrustManager {
                @Throws(CertificateException::class)
                override fun checkClientTrusted(
                    chain: Array<java.security.cert.X509Certificate>,
                    authType: String
                ) {
                }

                @Throws(CertificateException::class)
                override fun checkServerTrusted(
                    chain: Array<java.security.cert.X509Certificate>,
                    authType: String
                ) {
                }

                override fun getAcceptedIssuers(): Array<java.security.cert.X509Certificate> {
                    return arrayOf()
                }
            })
            val sslContext = SSLContext.getInstance("SSL")
            sslContext.init(null, trustAllCerts, java.security.SecureRandom())
            val sslSocketFactory = sslContext.socketFactory

            val builder = OkHttpClient.Builder().apply {
                interceptor?.let { addInterceptor(it) }
                sslSocketFactory(sslSocketFactory, trustAllCerts[0] as X509TrustManager)
                hostnameVerifier(HostnameVerifier { hostname, session -> true })
            }
            return builder.build()
        } catch (e: Exception) {
            throw RuntimeException(e)
        }
    }


    fun retrofit(endPointHttps:String, interceptor: Interceptor = AccessTokenInterceptor()): Retrofit {
        return Retrofit.Builder()
            .baseUrl( endPointHttps)
            .client(okHttpSsl(interceptor))
            .addConverterFactory(MoshiConverterFactory.create())
            .addCallAdapterFactory(CoroutineCallAdapterFactory())
            .build()
    }
}