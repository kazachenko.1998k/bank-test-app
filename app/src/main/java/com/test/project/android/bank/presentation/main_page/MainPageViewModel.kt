package com.test.project.android.bank.presentation.main_page

import android.app.Application
import androidx.lifecycle.MutableLiveData
import com.test.project.android.bank.domain.Card
import com.test.project.android.bank.domain.Valute
import com.test.project.android.bank.framework.BankViewModel
import com.test.project.android.bank.framework.Interactors
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class MainPageViewModel(application: Application, interactors: Interactors) : BankViewModel
(application, interactors) {

    val selectedCard = MutableLiveData<Card?>()

    val selectedValute = MutableLiveData<Valute?>().apply {
        GlobalScope.launch {
                postValue(interactors.getValute(Valute.EUR.second))
        }
    }

    suspend fun updateValute(value: String) {
        selectedValute.postValue(interactors.getValute(value))
    }

    val cards = MutableLiveData<List<Card>>().apply {
        GlobalScope.launch {
            val cards = interactors.getCards()
            postValue(cards)
            selectedCard.postValue(cards?.first())
        }
    }

    fun updateSelectedCards(card: Card) {
        selectedCard.postValue(card)
    }

}
