package com.test.project.android.bank.presentation

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.test.project.android.bank.R
import com.test.project.android.bank.domain.Card
import com.test.project.android.bank.presentation.list_cards.FragmentListCards
import com.test.project.android.bank.presentation.main_page.FragmentMainPage

class MainActivity : AppCompatActivity(),
        MainActivityDelegate {

    private var fragmentListCards: FragmentListCards? = null
    private var fragmentMainPage: FragmentMainPage? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.AppTheme)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        startApp()
    }

    override fun selectCard(card: Card) {
        if (fragmentListCards == null) {
            fragmentListCards = FragmentListCards.newInstance()
        }
        fragmentListCards!!.updateCard(card)
        supportFragmentManager.beginTransaction()
                .setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left, android.R.anim.slide_in_left, android.R.anim.slide_out_right)
                .replace(R.id.content, fragmentListCards!!)
                .addToBackStack(null)
                .commit()
    }

    private fun startApp() {
        if (fragmentMainPage == null) {
            fragmentMainPage = FragmentMainPage.newInstance()
        }
        supportFragmentManager.beginTransaction()
                .replace(R.id.content, fragmentMainPage!!)
                .addToBackStack(null)
                .commit()
    }

    override fun notifySelectedCard(card: Card) {
        onBackPressed()
        if (fragmentMainPage == null) {
            fragmentMainPage = FragmentMainPage.newInstance()
        }
        fragmentMainPage!!.updateCard(card)
    }

    override fun onBack() {
        onBackPressed()
    }

    override fun onBackPressed() {
        if (supportFragmentManager.fragments.size > 1) {
            supportFragmentManager.popBackStack()
        } else {
            finish()
        }
    }
}
