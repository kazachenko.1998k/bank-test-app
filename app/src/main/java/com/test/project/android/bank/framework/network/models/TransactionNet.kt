package com.test.project.android.bank.framework.network.models

import com.squareup.moshi.Json


data class TransactionNet(
        @Json(name = "title")
        val title: String = "",

        @Json(name = "icon_url")
        val icon_url: String? = null,

        @Json(name = "date")
        val date: String = "",

        @Json(name = "amount")
        val amount: String = ""
)
