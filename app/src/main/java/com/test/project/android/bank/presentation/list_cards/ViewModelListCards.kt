package com.test.project.android.bank.presentation.list_cards

import android.app.Application
import androidx.lifecycle.MutableLiveData
import com.test.project.android.bank.domain.Card
import com.test.project.android.bank.framework.Interactors
import com.test.project.android.bank.framework.BankViewModel
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class ViewModelListCards(application: Application, interactors: Interactors)
    : BankViewModel(application, interactors) {

    val cards: MutableLiveData<List<Card>> = MutableLiveData()
    val selectedCard: MutableLiveData<Card> = MutableLiveData()

    fun loadDocuments() {
        GlobalScope.launch {
            cards.postValue(interactors.getCards())
        }
    }

    fun updateSelectedCards(card: Card){
        selectedCard.postValue(card)
    }

}
