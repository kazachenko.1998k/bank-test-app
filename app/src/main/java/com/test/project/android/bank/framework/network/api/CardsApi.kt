package com.test.project.android.bank.framework.network.api

import com.test.project.android.bank.framework.network.models.UsersNet
import io.reactivex.Single
import kotlinx.coroutines.Deferred
import retrofit2.http.GET

interface CardsApi {

    @GET("users.json")
    fun getAllCardsAsync(): Deferred<UsersNet?>?
}