package com.test.project.android.bank.presentation.main_page

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.bumptech.glide.Glide
import com.test.project.android.bank.R
import com.test.project.android.bank.domain.Card
import com.test.project.android.bank.domain.Valute
import com.test.project.android.bank.framework.BankViewModelFactory
import com.test.project.android.bank.presentation.MainActivityDelegate
import com.test.project.android.bank.presentation.format
import com.test.project.android.bank.presentation.getIcon
import com.test.project.android.bank.presentation.main_page.custom.RadioIndicatorButton
import kotlinx.android.synthetic.main.element_bank_card.view.*
import kotlinx.android.synthetic.main.element_history.view.*
import kotlinx.android.synthetic.main.fragment_main_page.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.util.*

class FragmentMainPage : Fragment() {

    companion object {
        fun newInstance() = FragmentMainPage()
    }


    private var valute: Valute? = null
    private var card: Card? = null
    private var adapter: HistoryAdapter? = null
    private lateinit var viewModel: MainPageViewModel
    private lateinit var mainActivityDelegate: MainActivityDelegate

    override fun onAttach(context: Context) {
        super.onAttach(context)

        try {
            mainActivityDelegate = context as MainActivityDelegate
        } catch (e: ClassCastException) {
            throw ClassCastException()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_main_page, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        adapter = HistoryAdapter(mutableListOf(), Valute.EMPTY)
        history_recycler.recycler_view.adapter = adapter

        viewModel = ViewModelProviders.of(this, BankViewModelFactory)
                .get(MainPageViewModel::class.java)

        viewModel.selectedCard.observe(viewLifecycleOwner, Observer {
            if (it != null) {
                card = it
                updateView()
            }
        })

        viewModel.selectedValute.observe(viewLifecycleOwner, Observer {
            if (it != null) {
                valute = it
                updateView()
            }
        })
        (radio_group.getChildAt(1) as RadioIndicatorButton).isChecked = true
        radio_btn_gbr.setOnClickListener {
            updateValute(it.id)
        }
        radio_btn_eur.setOnClickListener {
            updateValute(it.id)
        }
        radio_btn_rub.setOnClickListener {
            updateValute(it.id)
        }
        bank_card.setOnClickListener {
            mainActivityDelegate.selectCard(card ?: Card.EMPTY)
        }

    }

    @SuppressLint("SetTextI18n")
    private fun updateView() {
        valute ?: return
        card ?: return
        bank_card.apply {
            Glide.with(this).load(card!!.getIcon())
                    .into(image_view_card_icon)
            Glide.with(this).load(R.drawable.ic_card_holder)
                    .into(image_view_card_holder_icon)
            text_view_card_number.text = card!!.cardNumber
            text_view_card_holder_name.text = card!!.cardholderName
            text_view_card_balance_current.text = valute!!.symbol() + (card!!.balance * (valute!!.value
                    ?: -1.0) / valute!!.nominal).format(Locale.ITALY)
            text_view_card_balance_default.text = valute!!.default() + card!!.balance.format()
            text_view_end_date.text = card!!.valid
            (radio_group.getChildAt(when (valute!!.charCode) {
                Valute.GBP.second -> 0
                Valute.EUR.second -> 1
                Valute.RUB.second -> 2
                else -> 1
            }) as RadioIndicatorButton).isChecked = true
        }
        adapter?.update(card!!.transactionHistory, valute!!)
    }

    private fun updateValute(id: Int) {
        GlobalScope.launch(Dispatchers.IO) {
            when (id) {
                R.id.radio_btn_eur -> viewModel.updateValute(Valute.EUR.second)
                R.id.radio_btn_gbr -> viewModel.updateValute(Valute.GBP.second)
                R.id.radio_btn_rub -> viewModel.updateValute(Valute.RUB.second)
            }
        }
    }

    fun updateCard(card: Card) {
        viewModel.updateSelectedCards(card)
    }
}
