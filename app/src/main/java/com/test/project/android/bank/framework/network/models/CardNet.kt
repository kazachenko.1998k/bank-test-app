package com.test.project.android.bank.framework.network.models

import com.squareup.moshi.Json

data class CardNet(
        @Json(name = "card_number")
        val card_number: String = "",

        @Json(name = "type")
        val type: String = "",

        @Json(name = "cardholder_name")
        val cardholder_name: String = "",

        @Json(name = "valid")
        val valid: String = "",

        @Json(name = "balance")
        val balance: Double = 0.0,

        @Json(name = "transaction_history")
        var transaction_history: List<TransactionNet> = mutableListOf()
)