package com.test.project.android.bank.presentation.list_cards

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.test.project.android.bank.R
import com.test.project.android.bank.domain.Card
import com.test.project.android.bank.presentation.getIcon
import kotlinx.android.synthetic.main.element_item_recycler_my_cards.view.*

class AdapterListCards(
        private val cards: MutableList<Card> = mutableListOf(),
        private var selectedCard: Card = Card.EMPTY,
        private val itemClickListener: (Card) -> Unit
) : RecyclerView.Adapter<AdapterListCards.ViewHolder>() {

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
                LayoutInflater.from(parent.context)
                        .inflate(R.layout.element_item_recycler_my_cards, parent, false)
        )
    }

    override fun getItemCount() = cards.size
    override fun getItemId(position: Int): Long {
        return cards[position].cardNumber.hashCode().toLong()
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val card = cards[position]
        holder.itemView.apply {
            Glide.with(this).load(card.getIcon())
                    .into(image_view_card_icon)
            text_view_card_number.text = card.cardNumber
            image_view_selected_card.visibility = if (card.cardNumber == selectedCard.cardNumber) View.VISIBLE else View.GONE
            setOnClickListener { itemClickListener.invoke(card) }
        }
    }

    fun update(newCards: List<Card>) {
        cards.clear()
        cards.addAll(newCards)
        notifyDataSetChanged()
    }

    fun update(newSelectedCard: Card) {
        selectedCard = newSelectedCard
        notifyDataSetChanged()
    }




}