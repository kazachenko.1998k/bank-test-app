package com.test.project.android.bank.framework.db.ds

import android.content.Context
import com.test.project.android.bank.data.TransactionDataSource
import com.test.project.android.bank.domain.Card
import com.test.project.android.bank.domain.Transaction
import com.test.project.android.bank.framework.db.BankDatabase
import com.test.project.android.bank.framework.db.TransactionEntity

class RoomTransactionDataSource(context: Context) : TransactionDataSource {

    private val transactionDao = BankDatabase.getInstance(context).transactionDao()

    override suspend fun add(transaction: Transaction, card: Card) = transactionDao.addTransaction(TransactionEntity(
            amount = transaction.amount,
            cardId = card.cardNumber,
            date = transaction.date,
            iconUrl = transaction.iconUrl,
            title = transaction.title
    ))

    override suspend fun get(card: Card): List<Transaction> = transactionDao.getTransactions(card.cardNumber).map { transaction ->
        Transaction(
                amount = transaction.amount,
                date = transaction.date,
                iconUrl = transaction.iconUrl,
                title = transaction.title
        )
    }

    override suspend fun clear(card: Card) = transactionDao.clearTransactionsOnCard(card.cardNumber)

}