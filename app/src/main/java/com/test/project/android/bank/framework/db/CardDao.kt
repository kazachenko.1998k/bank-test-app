package com.test.project.android.bank.framework.db

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy.REPLACE
import androidx.room.Query

@Dao
interface CardDao {

    @Insert(onConflict = REPLACE)
    suspend fun addCard(user: CardEntity)

    @Query("SELECT * FROM cards")
    suspend fun getCards(): List<CardEntity>

    @Query("SELECT * FROM cards WHERE card_number = :number")
    suspend fun getCard(number: String): CardEntity?

    @Query("SELECT * FROM `transaction` WHERE card_id = :cardId")
    suspend fun getTransactions(cardId: String): List<TransactionEntity>

}