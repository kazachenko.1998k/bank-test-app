package com.test.project.android.bank.framework.network.api

import com.test.project.android.bank.framework.network.models.ValutesNet
import io.reactivex.Single
import kotlinx.coroutines.Deferred
import retrofit2.http.GET

interface ValuteApi {

    @GET("daily_json.js")
    fun getAllValutes(): Deferred<ValutesNet?>?
}