package com.test.project.android.bank.framework.network.models

import com.squareup.moshi.Json

data class ValutesNet(
        @Json(name = "Date") val Date: String = "",
        @Json(name = "PreviousDate") val PreviousDate: String = "",
        @Json(name = "PreviousURL") val PreviousURL: String = "",
        @Json(name = "Timestamp") val Timestamp: String = "",
        @Json(name = "Valute") val Valute: MutableMap<String, ValuteNet> = mutableMapOf()
)