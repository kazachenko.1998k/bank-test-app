package com.test.project.android.bank.framework.network

import android.util.Log
import okhttp3.Interceptor
import okhttp3.Response

class AccessTokenInterceptor(
) : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {

        val authenticatedRequest = chain.request().newBuilder()

        authenticatedRequest
            .addHeader("Access-Control-Allow-Origin", "*")
            .addHeader("content-type", "application/x-www-form-urlencoded")

        Log.d("REQUEST", authenticatedRequest.build().headers().toString())
        val result = chain.proceed(authenticatedRequest.build())
        Log.d("RESPONSE", result.toString())
        return result
    }


}