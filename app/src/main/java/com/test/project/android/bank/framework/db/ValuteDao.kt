package com.test.project.android.bank.framework.db

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy.REPLACE
import androidx.room.Query

@Dao
interface ValuteDao {

    @Insert(onConflict = REPLACE)
    suspend fun addValute(bookmark: ValuteEntity)

    @Query("SELECT * FROM valute")
    suspend fun getValutes(): List<ValuteEntity>

    @Query("SELECT * FROM valute WHERE char_code = :charCode")
    suspend fun getValute(charCode: String): ValuteEntity?
}