package com.test.project.android.bank.framework

import com.test.project.android.bank.data.CardDataSource
import com.test.project.android.bank.domain.Card
import com.test.project.android.bank.framework.db.ds.RoomCardDataSource
import com.test.project.android.bank.framework.db.ds.RoomTransactionDataSource
import com.test.project.android.bank.framework.network.ds.NetCardDataSource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class CardDataSourceImpl(private val netCardDataSource: NetCardDataSource, private val roomCardDataSource: RoomCardDataSource, private val roomTransactionDataSource: RoomTransactionDataSource) : CardDataSource {

    override suspend fun add(card: Card) = roomCardDataSource.add(card)

    override suspend fun get(): List<Card> {
        return withContext(Dispatchers.IO) {
            getNet()
            roomCardDataSource.get()
        }
    }

    override suspend fun getByNumber(number: String): Card {
        return withContext(Dispatchers.IO) {
            getNet()
            roomCardDataSource.getByNumber(number)
        }
    }

    suspend fun getNet(){
        val net = netCardDataSource.get()
        if (!net.isNullOrEmpty()) {
            net.forEach { card ->
                roomTransactionDataSource.clear(card)
                add(card)
                card.transactionHistory.forEach { transaction ->
                    roomTransactionDataSource.add(transaction, card)
                }
            }
        }
    }
}