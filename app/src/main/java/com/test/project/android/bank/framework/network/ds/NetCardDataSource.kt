package com.test.project.android.bank.framework.network.ds

import com.test.project.android.bank.data.CardDataSource
import com.test.project.android.bank.domain.Card
import com.test.project.android.bank.domain.Transaction
import com.test.project.android.bank.framework.network.NetworkModule
import com.test.project.android.bank.framework.network.api.CardsApi

class NetCardDataSource : CardDataSource {

    private val cardsApi by lazy { NetworkModule.retrofit(NetworkModule.CARDS_URL).create(CardsApi::class.java) }

    override suspend fun add(card: Card) {
        TODO("Not yet implemented")
    }

    override suspend fun get(): List<Card>? = cardsApi.getAllCardsAsync()?.await()?.users?.map { card ->
        Card(cardNumber = card.card_number,
                balance = card.balance,
                cardholderName = card.cardholder_name,
                type = card.type,
                valid = card.valid, transactionHistory = card.transaction_history.map { transaction ->
            Transaction(
                    amount = transaction.amount,
                    date = transaction.date,
                    iconUrl = transaction.icon_url,
                    title = transaction.title
            )
        })
    }

    override suspend fun getByNumber(number: String): Card? = get()?.find { it.cardNumber == number }
            ?: Card.EMPTY
}