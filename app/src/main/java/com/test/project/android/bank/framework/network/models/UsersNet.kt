package com.test.project.android.bank.framework.network.models

import com.squareup.moshi.Json

data class UsersNet(
        @Json(name = "users")
        var users: MutableList<CardNet> = mutableListOf()
)