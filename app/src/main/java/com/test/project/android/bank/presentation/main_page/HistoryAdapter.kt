package com.test.project.android.bank.presentation.main_page

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.test.project.android.bank.R
import com.test.project.android.bank.domain.Transaction
import com.test.project.android.bank.domain.Valute
import com.test.project.android.bank.presentation.format
import kotlinx.android.synthetic.main.element_item_recycler_history.view.*

class HistoryAdapter(
        private val transactions: MutableList<Transaction> = mutableListOf(),
        private var valute: Valute
) : RecyclerView.Adapter<HistoryAdapter.ViewHolder>() {

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
                LayoutInflater.from(parent.context)
                        .inflate(R.layout.element_item_recycler_history, parent, false)
        )
    }

    override fun getItemCount() = transactions.size

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val transaction = transactions[position]
        holder.itemView.apply {
            Glide.with(this).load(transaction.iconUrl)
                    .into(image_view_card_holder_icon)
            text_view_history_name.text = transaction.title
            text_view_history_date.text = transaction.date
            text_view_history_value_current.text = valute.symbol() + (transaction.amount.toDouble() * (valute.value
                    ?: -1.0) / valute.nominal).format()
            text_view_history_value_default.text = valute.default() + transaction.amount
        }
    }

    fun update(newTransactions: List<Transaction>, newValute: Valute) {
        transactions.clear()
        transactions.addAll(newTransactions)
        valute = newValute
        notifyDataSetChanged()
    }

}