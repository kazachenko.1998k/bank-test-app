package com.test.project.android.bank.presentation

import com.test.project.android.bank.R
import com.test.project.android.bank.domain.Card
import java.util.*

fun Card.getIcon(): Int? {
    return TYPES.values().find { type -> type.value == this.type }?.icon
}

fun Double.format(locale: Locale = Locale.US, digits: Int = 2) = "%.${digits}f".format(locale,this)


enum class TYPES(val value: String, val icon: Int) {
    VISA("visa", R.drawable.ic_visa),
    UNION_PAY("unionpay", R.drawable.ic_union_pay),
    MASTERCARD("mastercard", R.drawable.ic_mastercard)
}