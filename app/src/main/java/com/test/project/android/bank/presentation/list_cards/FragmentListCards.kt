package com.test.project.android.bank.presentation.list_cards

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.test.project.android.bank.R
import com.test.project.android.bank.domain.Card
import com.test.project.android.bank.framework.BankViewModelFactory
import com.test.project.android.bank.presentation.MainActivityDelegate
import kotlinx.android.synthetic.main.fragment_my_cards.*

class FragmentListCards : Fragment() {
    private var card: Card? = null

    companion object {
        fun newInstance() = FragmentListCards()
    }

    private var viewModel: ViewModelListCards? = null

    private lateinit var mainActivityDelegate: MainActivityDelegate

    override fun onAttach(context: Context) {
        super.onAttach(context)

        try {
            mainActivityDelegate = context as MainActivityDelegate
        } catch (e: ClassCastException) {
            throw ClassCastException()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_my_cards, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        val adapter = AdapterListCards {
            mainActivityDelegate.notifySelectedCard(it)
        }
        recycler_view.adapter = adapter
        recycler_view.setHasFixedSize(true)
        toolbar.setNavigationOnClickListener {
            mainActivityDelegate.onBack()
        }
        viewModel = ViewModelProviders.of(this, BankViewModelFactory)
                .get(ViewModelListCards::class.java)
        viewModel!!.cards.observe(viewLifecycleOwner, Observer {
            adapter.update(it)
        })
        viewModel!!.loadDocuments()
        updateCard(card ?: Card.EMPTY)
        viewModel!!.selectedCard.observe(viewLifecycleOwner, Observer {
            adapter.update(it)
        })
    }

    fun updateCard(card: Card) {
        this.card = card
        viewModel?.updateSelectedCards(card)
    }
}
