package com.test.project.android.bank.framework

import com.test.project.android.bank.interactors.*

data class Interactors(
        val addCard:AddCard,
        val addTransaction:AddTransaction,
        val addValute:AddValute,
        val clearTransactionsOnCard:ClearTransactionsOnCard,
        val getCards:GetCards,
        val getTransactions:GetTransactions,
        val getValutes:GetValutes,
        val getValute:GetValute
)